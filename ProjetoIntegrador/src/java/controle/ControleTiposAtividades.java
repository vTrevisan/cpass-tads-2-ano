/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controle;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import modelo.TipoAtividade;
import util.DAOGenerico;


/**
 *
 * @author Mauricio
 */
@ManagedBean
@ViewScoped
public class ControleTiposAtividades {

    private TipoAtividade objetoTipoAtividade = new TipoAtividade();
    private DAOGenerico dao = new DAOGenerico();
    private List<TipoAtividade> listaTipoAtividades = new ArrayList<>();

    public ControleTiposAtividades() {
        preencher();
    }

    public List<TipoAtividade> getListaTipoAtividade() {
        if (listaTipoAtividades == null) {
            preencher();
        }
        return listaTipoAtividades;
    }

    public void setListaTipoAtividade(List<TipoAtividade> listaTipoAtiidade) {
        this.listaTipoAtividades = listaTipoAtiidade;
    }

    public TipoAtividade getObjetoTipoAtividade() {

        return objetoTipoAtividade;
    }

    public void setObjetoTipoAtividade(TipoAtividade objTipoAtividade) {
        this.objetoTipoAtividade = objTipoAtividade;
    }

    public void excluir(TipoAtividade tipoAtividade) {
        objetoTipoAtividade = tipoAtividade;

        if (objetoTipoAtividade.getId() != null) {
            try {
                System.out.println("Entro no excluir");
                dao.excluir(objetoTipoAtividade);

            } catch (Exception e) {

            }

        }
        preencher();
        novo();
    }

    public void novo() {
        objetoTipoAtividade = new TipoAtividade();
    }

    public void inserir() {
        if (objetoTipoAtividade.getId() == null) {
            dao.inserir(objetoTipoAtividade);
        } else {
            dao.alterar(objetoTipoAtividade);
        }
        objetoTipoAtividade = new TipoAtividade();
        preencher();

    }

    public void preencher() {
        listaTipoAtividades = dao.lista(TipoAtividade.class);
    }

    public void excluirTipo() {

        if (objetoTipoAtividade.getId() != null) {
            try {
                dao.excluir(objetoTipoAtividade);

            } catch (Exception e) {

            }
        }
        preencher();
        novo();

    }

    //---lista
    public List<TipoAtividade> completeSegmento(String query) {
        List<TipoAtividade> all = dao.lista(TipoAtividade.class);
        List<TipoAtividade> filtered = new ArrayList<TipoAtividade>();

        for (int i = 0; i < all.size(); i++) {
            TipoAtividade skin = all.get(i);
            if (skin.getDescricao().toLowerCase().startsWith(query)) {
                filtered.add(skin);
            }
        }
        return filtered;
    }
}
