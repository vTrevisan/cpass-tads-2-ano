/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import modelo.Avaliador;
import modelo.Local;
import modelo.Segmento;
import util.DAOGenerico;

/**
 *
 * @author SERVIDOR
 */
@ManagedBean
@ViewScoped
public class ControleAvaliador {

    private Avaliador objetoAvaliador = new Avaliador();
    private DAOGenerico dao = new DAOGenerico();
    private List<Avaliador> listaAvaliador = new ArrayList<>();

    public ControleAvaliador() {
        preencher();
    }

    public List<Segmento> completaSegmento(String query) {
        List<Segmento> segCadastrados = dao.lista(Segmento.class);
        List<Segmento> segRelacionados = new ArrayList<Segmento>();

        for (Segmento seg : segCadastrados) {
            if (seg.getDescricao().toLowerCase().startsWith(query)) {
                segRelacionados.add(seg);
            }
        }

        return segRelacionados;
    }

    public void excluir(Avaliador avaliador) {
        objetoAvaliador = avaliador;

        if (objetoAvaliador.getId() != null) {
            try {
                System.out.println("Entro no excluir");
                dao.excluir(objetoAvaliador);

            } catch (Exception e) {

            }

        }
        preencher();
        novo();
    }

    public void novo() {
        objetoAvaliador = new Avaliador();
    }

    public void inserir() {
        if (objetoAvaliador.getId() == null) {
            dao.inserir(objetoAvaliador);
        } else {
            dao.alterar(objetoAvaliador);
        }
        objetoAvaliador = new Avaliador();
        preencher();

    }

    public void preencher() {
        listaAvaliador = dao.lista(Avaliador.class);
    }

    public void excluirTipo() {

        if (objetoAvaliador.getId() != null) {
            try {
                dao.excluir(objetoAvaliador);

            } catch (Exception e) {

            }
        }
        preencher();
        novo();

    }

    public Avaliador getObjetoAvaliador() {
        return objetoAvaliador;
    }

    public void setObjetoAvaliador(Avaliador objetoAvaliador) {
        this.objetoAvaliador = objetoAvaliador;
    }

    public List<Avaliador> getListaAvaliador() {
        return listaAvaliador;
    }

    public void setListaAvaliador(List<Avaliador> listaAvaliador) {
        this.listaAvaliador = listaAvaliador;
    }

}
