package controle;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import modelo.Atividade;
import util.DAOGenerico;

/**
 *
 * @author Raphael
 */
@ManagedBean(name = "atividadeMB")
@RequestScoped
public class ControleAtividade {

    private Atividade objAtividade = new Atividade();
    private DAOGenerico dao = new DAOGenerico();
    private List<Atividade> lista = new ArrayList<Atividade>();

    public ControleAtividade() {
        preencher();
    }

//--gets e sets

    public DAOGenerico getDao() {
        return dao;
    }

    public void setDao(DAOGenerico dao) {
        this.dao = dao;
    }
    
    public Atividade getObjAtividade() {
        return objAtividade;
    }

    public void setObjAtividade(Atividade objAtividade) {
        this.objAtividade = objAtividade;
    }

    public List<Atividade> getLista() {
        if (lista == null) {
            preencher();
        }
        return lista;
    }

    public void setLista(List<Atividade> lista) {
        this.lista = lista;
    }

//---metodos
    public void inserir() {
        if (objAtividade.getId() == null) {
            dao.inserir(objAtividade);
        } else {
            dao.alterar(objAtividade);
        }
        preencher();
        novo();
    }

    public void preencher() {
        lista = dao.lista(Atividade.class);
    }

    public void novo() {
        objAtividade = new Atividade();
    }

    public void excluir(Atividade atividade) {
        atividade = objAtividade;
        if (objAtividade != null) {
            try {
                dao.excluir(objAtividade);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        preencher();
        novo();
    }

//---lista
    public List<Atividade> completeAtividade(String query) {
        List<Atividade> all = dao.lista(Atividade.class);
        List<Atividade> filtered = new ArrayList<Atividade>();

        for (int i = 0; i < all.size(); i++) {
            Atividade skin = all.get(i);
            if (skin.getTitulo().toLowerCase().startsWith(query)) {
                filtered.add(skin);
            }
        }
        return filtered;
    }

}
