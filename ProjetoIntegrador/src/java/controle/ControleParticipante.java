/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controle;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import modelo.Participante;
import util.DAOGenerico;

/**
 *
 * @author SERVIDOR
 */
@ManagedBean
@ViewScoped
public class ControleParticipante {

    private DAOGenerico daoP = new DAOGenerico();
    private Participante objetoParticipante = new Participante();
    private List<Participante> listaParticipante = new ArrayList<Participante>();

    public void inserir() {
        if (objetoParticipante.getId() == null) {
            daoP.inserir(objetoParticipante);
        } else {
            daoP.alterar(objetoParticipante);
        }
        preencher();
        novo();
    }

    public void preencher() {
        listaParticipante = daoP.lista(Participante.class);
    }

    public void novo() {
        objetoParticipante = new Participante();
    }

    public void excluir(Participante participante) {
        participante = objetoParticipante;
        if (objetoParticipante != null) {
            try {
                daoP.excluir(objetoParticipante);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        preencher();
        novo();
    }

    public Participante getObjetoParticipante() {
        return objetoParticipante;
    }

    public void setObjetoParticipante(Participante objetoParticipante) {
        this.objetoParticipante = objetoParticipante;
    }

    public List<Participante> getListaParticipante() {
        if (listaParticipante == null) {
            preencher();
        }
        return listaParticipante;
    }

    public void setListaParticipante(List<Participante> listaParticipante) {
        this.listaParticipante = listaParticipante;
    }

}
