/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controle;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import modelo.Local;
import util.DAOGenerico;

/**
 *
 * @author User
 */
@ManagedBean
@ViewScoped
public class ControleLocal {

    private DAOGenerico dao = new DAOGenerico();
    private List<Local> listaLocal = new ArrayList<>();
    private Local objetoLocal = new Local();

    public ControleLocal() {
        preencher();
    }

    public void preencher() {
        listaLocal = dao.lista(Local.class);
    }

    public void inserir() {
        if (objetoLocal.getId() == null) {
            dao.inserir(objetoLocal);
        } else {
            dao.alterar(objetoLocal);
        }
        preencher();
        novo();
    }

    public void excluir(Local local) {
        objetoLocal = local;
        if (objetoLocal.getId() != null) {
            try {
                dao.excluir(objetoLocal);
            } catch (Exception e) {
            }
        }
        preencher();
        novo();

    }

    public void novo() {
        objetoLocal = new Local();
    }

    public List<Local> getListaLocal() {
        if (listaLocal == null) {
            preencher();
        }
        return listaLocal;
    }

    public void setListaLocal(List<Local> listaLocal) {
        this.listaLocal = listaLocal;
    }

    public Local getObjetoLocal() {
        return objetoLocal;
    }

    public void setObjetoLocal(Local objetoLocal) {
        this.objetoLocal = objetoLocal;
    }

    public void excluirLocal() {
        if (objetoLocal.getId() != null) {
            try {
                dao.excluir(objetoLocal);
            } catch (Exception e) {
            }
        }
        preencher();
        novo();

    }

    //---lista autocomplete
    public List<Local> completeLocal(String query) {
        List<Local> all = dao.lista(Local.class);
        List<Local> filtered = new ArrayList<Local>();

        for (int i = 0; i < all.size(); i++) {
            Local skin = all.get(i);
            if (skin.getDescricao().toLowerCase().startsWith(query)) {
                filtered.add(skin);
            }
        }
        return filtered;
    }
}
