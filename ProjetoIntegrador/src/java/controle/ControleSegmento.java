/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controle;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import modelo.Segmento;
import util.DAOGenerico;

/**
 *
 * @author User
 */
@ManagedBean
@ViewScoped
public class ControleSegmento {

    DAOGenerico daoSeg = new DAOGenerico();
    Segmento objetoSegmento = new Segmento();
    List<Segmento> listaSegmento = new ArrayList<>();

    public ControleSegmento() {
        preencher();
    }

    public void novo() {
        objetoSegmento = new Segmento();
    }

    public void inserir() {
        if (objetoSegmento == null) {
            daoSeg.inserir(objetoSegmento);
        } else {
            daoSeg.alterar(objetoSegmento);
        }
        objetoSegmento = new Segmento();
        preencher();

    }

    public void excluir(Segmento segm) {
        objetoSegmento = segm;
        if (objetoSegmento.getId() != null) {
            try {
                daoSeg.excluir(objetoSegmento);

            } catch (Exception e) {

            }
        }
        preencher();
        novo();

    }

    public void preencher() {
        listaSegmento = daoSeg.lista(Segmento.class);
    }

    public Segmento getObjetoSegmento() {
        return objetoSegmento;
    }

    public void setObjetoSegmento(Segmento objetoSegmento) {
        this.objetoSegmento = objetoSegmento;
    }

    public List<Segmento> getListaSegmento() {
        if (listaSegmento == null) {
            preencher();
        }
        return listaSegmento;
    }

    public void setListaSegmento(List<Segmento> listaSegmento) {
        this.listaSegmento = listaSegmento;
    }

    public void excluirSegmento() {

        if (objetoSegmento.getId() != null) {
            try {
                daoSeg.excluir(objetoSegmento);

            } catch (Exception e) {

            }
        }
        preencher();
        novo();
    }
    
    //---lista
    public List<Segmento> completeSegmento(String query) {
        List<Segmento> all = daoSeg.lista(Segmento.class);
        List<Segmento> filtered = new ArrayList<Segmento>();

        for (int i = 0; i < all.size(); i++) {
            Segmento skin = all.get(i);
            if (skin.getDescricao().toLowerCase().startsWith(query)) {
                filtered.add(skin);
            }
        }
        return filtered;
    }
}
