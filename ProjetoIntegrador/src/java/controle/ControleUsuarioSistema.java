package controle;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

import modelo.UsuarioSistema;
import util.DAOGenerico;

@ManagedBean
@ViewScoped
public class ControleUsuarioSistema {

    private String pesquisa;
    private List<UsuarioSistema> listaUsuarioSistema;
    private UsuarioSistema UsuarioSistema;
    private DAOGenerico dao;
    boolean frmLista;
    boolean frmCadastro;

    public ControleUsuarioSistema() {
        pesquisa = "";
        dao = new DAOGenerico();
        listaUsuarioSistema = (List<UsuarioSistema>) dao.lista(UsuarioSistema.class);
        UsuarioSistema = new UsuarioSistema();
        frmLista = true;
        frmCadastro = false;
    }

    public void novoRegistro() {
        UsuarioSistema = new UsuarioSistema();
    }

    public void salvarAlterar() {
        if (UsuarioSistema.getId() == null || UsuarioSistema.getId() == 0) {
            dao.inserir(UsuarioSistema);
        } else {
            dao.alterar(UsuarioSistema);
        }
        listaUsuarioSistema = dao.lista(UsuarioSistema.class);
    }

    public void excluir(UsuarioSistema obj) {
        dao.excluir(obj);
        listaUsuarioSistema = dao.lista(UsuarioSistema.class);
    }

    public void pesquisar() {
        if (pesquisa.trim().length() > 0) {

        }
    }

    public List<UsuarioSistema> getListaUsuarioSistema() {
        return listaUsuarioSistema;
    }

    public void setListaUsuarioSistema(List<UsuarioSistema> listaUsuarioSistema) {
        this.listaUsuarioSistema = listaUsuarioSistema;
    }

    public UsuarioSistema getUsuarioSistema() {
        return UsuarioSistema;
    }

    public void setUsuarioSistema(UsuarioSistema UsuarioSistema) {
        this.UsuarioSistema = UsuarioSistema;
    }

    public String getPesquisa() {
        return pesquisa;
    }

    public void setPesquisa(String pesquisa) {
        this.pesquisa = pesquisa;
    }

    public boolean isFrmLista() {
        return frmLista;
    }

    public void setFrmLista(boolean frmLista) {
        this.frmLista = frmLista;
    }

    public boolean isFrmCadastro() {
        return frmCadastro;
    }

    public void setFrmCadastro(boolean frmCadastro) {
        this.frmCadastro = frmCadastro;
    }
}
