package conversor;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import modelo.TipoAtividade;
import util.DAOGenerico;

/**
 *
 * @author Raphael
 */
@FacesConverter("conversorTipoAtividade")
public class ConversorTipoAtividade implements Converter {

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        if (string != null && string.trim().length() > 0) {
            DAOGenerico dao = new DAOGenerico();
            return dao.recupera(TipoAtividade.class, Long.parseLong(string));
        } else {
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        if(o!=null){
            return String.valueOf(((TipoAtividade)o).getId());
        }else{
            return null;
        }
    }
}
