package conversor;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import modelo.Segmento;
import util.DAOGenerico;

/**
 *
 * @author Raphael
 */
@FacesConverter("conversorSegmento")
public class ConversorSegmento implements Converter {

    @Override
    public Object getAsObject(FacesContext fc, UIComponent uic, String string) {
        if (string != null && string.trim().length() > 0) {
            DAOGenerico dao = new DAOGenerico();
            return dao.recupera(Segmento.class, Long.parseLong(string));
        } else {
            return null;
        }
    }

    @Override
    public String getAsString(FacesContext fc, UIComponent uic, Object o) {
        if (o != null) {
            return String.valueOf(((Segmento) o).getId());
        } else {
            return null;
        }
    }
}
