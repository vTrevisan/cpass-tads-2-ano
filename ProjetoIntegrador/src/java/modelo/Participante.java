package modelo;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;

/**
 *
 * @author Raphael
 */
@Entity
public class Participante extends Pessoa implements Serializable{

    @Column(nullable = false)
    private Character tipoParticipante;

    public Character getTipoParticipante() {
        return tipoParticipante;
    }

    public void setTipoParticipante(Character tipoParticipante) {
        this.tipoParticipante = tipoParticipante;
    }

}
