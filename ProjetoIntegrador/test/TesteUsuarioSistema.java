
import java.util.Date;
import modelo.UsuarioSistema;
import org.junit.Test;
import util.DAOGenerico;

public class TesteUsuarioSistema {

    public TesteUsuarioSistema() {
    }

    @Test
    public void teste() {
        System.out.println("NovoEmptyJUnitTest.teste()");
        DAOGenerico dao = new DAOGenerico();
        UsuarioSistema obj = new UsuarioSistema();
        obj.setCpf("000.000.000-00");
        obj.setDtNasc(new Date());
        obj.setEmail("teste@tes.com");
        obj.setNome("Testador");
        obj.setPermissao('a');
        obj.setSenha("123");
        obj.setTelefone("44 3424 5500");
        dao.inserir(obj);
    }
}
